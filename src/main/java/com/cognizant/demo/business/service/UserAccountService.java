package com.cognizant.demo.business.service;

import com.cognizant.demo.business.bo.UserAccountDetailsRequestBO;
import com.cognizant.demo.business.bo.UserAccountDetailsResponseBO;

/**
 * User Account details Service Interface which provides user login and user
 * banking related services.
 * 
 * @author TarunKumar.Singh
 */
public interface UserAccountService {

	/**
	 * Service method which is used to register user and bank account.
	 * 
	 * @return {@link UserAccountDetailsResponseBO}
	 * @throws Exception (Errors occurred during execution of application.)
	 */
	UserAccountDetailsResponseBO registerUser(UserAccountDetailsRequestBO userAccountDetailsRequestBO) throws Exception;

	/**
	 * Service method which is used to get transaction limit of user.
	 * 
	 * @return {@link UserAccountDetailsResponseBO}
	 * @throws Exception (Errors occurred during execution of application.)
	 */
	UserAccountDetailsResponseBO getUserTransactionLimit(final String accessToken, final String accountNumber) throws Exception;
}
