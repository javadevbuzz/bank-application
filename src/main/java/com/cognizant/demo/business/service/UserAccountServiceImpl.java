package com.cognizant.demo.business.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.demo.business.bo.UserAccountDetailsRequestBO;
import com.cognizant.demo.business.bo.UserAccountDetailsResponseBO;
import com.cognizant.demo.business.dao.UserAccountDetailsDao;

/**
 * User Account details Service Interface which provides user login and user
 * banking related services.
 * 
 * @author TarunKumar.Singh
 */
public class UserAccountServiceImpl implements UserAccountService {

	@Autowired
	private UserAccountDetailsDao userAccountDetailsDao;

	/**
	 * Service method which is used to register user and bank account.
	 * 
	 * @return {@link UserAccountDetailsResponseBO}
	 * @throws Exception (Errors occurred during execution of application.)
	 */
	public UserAccountDetailsResponseBO registerUser(UserAccountDetailsRequestBO userAccountDetailsRequestBO)
			throws Exception {
		return null;
	}

	/**
	 * Service method which is used to get transaction limit of user.
	 * 
	 * @return {@link UserAccountDetailsResponseBO}
	 * @throws Exception (Errors occurred during execution of application.)
	 */
	public UserAccountDetailsResponseBO getUserTransactionLimit(final String accessToken, final String accountNumber)
			throws Exception {
		return null;
	}
}
