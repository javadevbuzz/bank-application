package com.cognizant.demo.business.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.demo.business.beans.UserAccountDetails;

/**
 * Interface containing {@link UserAccountDetails} related operations to interact with
 * Database.
 * 
 * @author TarunKumar.Singh
 */
@Repository
public interface UserAccountDetailsDao extends JpaRepository<UserAccountDetails, Integer> {

	/**
	 * DAO method to get the Transaction Limit of user.
	 * @param accessToken
	 * @param accountNumber
	 * @return
	 */
	@Query("select transactionLimit FROM UserAccountDetails WHERE accessToken=?1 and accountNumber=?2")
	public Integer getTransactionLimitByAuthTokenAndAccountNumber(final String accessToken, final String accountNumber);
}
