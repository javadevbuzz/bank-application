package com.cognizant.demo.common.util;

import java.util.UUID;

/**
 * This is a utility class containing utility methods like date conversion, date
 * formatting.
 * 
 * @author TarunKumar.Singh
 *
 */
public class MiscUtils {

	public static String getUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
}