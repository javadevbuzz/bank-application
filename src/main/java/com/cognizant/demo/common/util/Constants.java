package com.cognizant.demo.common.util;

public class Constants {

	// Date format patterns.
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String DATE_FORMAT_DDMMMYYYYHHMMSS = "dd MMM yyyy HH:mm:ss";
	public static final String DATE_FORMAT_MMMDDYYYYHHMMSS = "dd MMM yyyy";
	// This regex is for year format validation.
	public static final String REGEX_PATTERN_yyyyMMdd = "\\d{4}-[01]\\d-[0-9]\\d";
	public static final String UTC_TIMEZONE = "UTC";
	public static final String HYPHEN = "-";
	public static final String USER_IDENTIFICATION_STRING = "email";
	public static final String USER_SESSION_ATTRIBUTE_NAME = "USER";
	// This regex is for password validation.
	public static final String REGEX_PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,15}$";
	// Regex for email validation
	public static final String REGEX_EMAIL_PATTERN = "^[A-Z0-9._-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
	public static final String RESPONSE_MESSAGE = "message";
	public static final String RESPONSE_ERROR = "error";
	public static final int COOKIE_MAX_AGE = (30 * 24 * 60 * 60);

	public static final String EQUALS_TO_STRING = "=";
	
}