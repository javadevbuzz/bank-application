package com.cognizant.demo.common.util;


import static com.cognizant.demo.common.util.Constants.UTC_TIMEZONE;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;


/**
 * This is a utility class containing utility methods like date conversion, date
 * formatting.
 * 
 * @author TarunKumar.Singh
 *
 */
public class DateUtils {

	/**
	 * This method is used to convert date object {@link Date} in a given date
	 * format using SimpleDateFormat {@link SimpleDateFormat}
	 * 
	 * @param calendar
	 *            ({@link Date} object.)
	 * @param dateFormat
	 *            (Given date format {@link String}.)
	 * @return (Formatted date string {@link SimpleDateFormat}.)
	 */
	public static String getFormattedDateText(Date date, String dateFormat) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		return simpleDateFormat.format(date.getTime());
	}

	/**
	 * This method is used to convert calendar object {@link Calendar} in a
	 * given date format using SimpleDateFormat {@link SimpleDateFormat}
	 * 
	 * @param calendar
	 *            ({@link Calendar} object.)
	 * @param dateFormat
	 *            (Given date format {@link String}.)
	 * @return (Formatted date string {@link SimpleDateFormat}.)
	 */
	public static String getFormattedDateText(Calendar calendar, String dateFormat) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		return simpleDateFormat.format(calendar.getTime());
	}

	/**
	 * This method is used to convert calendar object {@link Calendar} in a
	 * given date format using SimpleDateFormat {@link SimpleDateFormat}
	 * 
	 * @param calendar
	 *            ({@link Calendar} object.)
	 * @param dateFormat
	 *            (Given date format {@link String}.)
	 * @return (Formatted date string {@link SimpleDateFormat}.)
	 */
	public static String getTodaysFormattedDateText(String dateFormat) {
		Calendar calendar = getTodaysCalendarInUTCTimeZone();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		return simpleDateFormat.format(calendar.getTime());
	}

	/**
	 * This method is returning current UTC calendar.
	 * 
	 * @return {@link Calendar}
	 */
	public static Calendar getTodaysCalendarInUTCTimeZone() {
		TimeZone.setDefault(TimeZone.getTimeZone(UTC_TIMEZONE));
		return Calendar.getInstance();
	}

	/**
	 * This method is used to covert util date {@link Date} to timestamp date
	 * {@link java.sql.Timestamp}
	 * 
	 * @param date
	 *            {@link Date} (Date object needs to be converted to timestamp.)
	 * @return (Converted date object in {@link Timestamp})
	 */
	public static Timestamp convertDateToTimestamp(Date date) {
		return new Timestamp(date.getTime());
	}

	/**
	 * This method is returning current UTC date and time.
	 * 
	 * @return currentUTCDate (Return currentUTCDate as {@link Date}})
	 */
	public static Date getCurrentDateAndTimeInUTCTimeZone() {
		TimeZone.setDefault(TimeZone.getTimeZone(Constants.UTC_TIMEZONE));
		return new Date(Calendar.getInstance().getTimeInMillis());
	}

	/**
	 * This method checks whether the date is in given format and can be parsed
	 * as a valid date.
	 * 
	 * @param inputDate
	 *            (Input date given by user {@link String})
	 * @param dateFormat
	 *            (Input date format needs to be given by user {@link String})
	 * @return (true if date pattern matches the requirement else false.
	 *         {@link Boolean})
	 */
	public static boolean isValidDate(String inputDate, String dateFormat) {
		if (inputDate == null || !inputDate.matches(Constants.REGEX_PATTERN_yyyyMMdd))
			return false;

		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		format.setLenient(false);
		try {
			format.parse(inputDate);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static Date getDateMMMddYYYY(String inputDate, String dateFormat) {
		if (inputDate == null)
			return null;

		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		format.setLenient(false);
		try {
		//	System.out.println(format.parse(inputDate));
			return format.parse(inputDate);
		} catch (ParseException e) {
			return null;
		}
	}

	public static Date getDateMMMddYYYYUTC(String inputDate, String dateFormat) {
		if (inputDate == null)
			return null;

		// Set default time zone to UTC time zone.
		TimeZone.setDefault(TimeZone.getTimeZone(Constants.UTC_TIMEZONE));

		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		format.setLenient(false);
		try {
			System.out.println(format.parse(inputDate));
			return format.parse(inputDate);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * This method checks whether the date is in given format or not.
	 * 
	 * @param inputDate
	 *            (Input date given by user {@link String})
	 * @param dateFormat
	 *            (Input date format needs to be given by user {@link String})
	 * @return (true if date pattern matches the requirement else false.
	 *         {@link Boolean})
	 */
	public static boolean isValidDateFormat(String inputDate, String dateFormat) {
		if (StringUtils.isNotEmpty(dateFormat) && StringUtils.equals(Constants.DATE_FORMAT, dateFormat)) {
			return (StringUtils.isNotEmpty(inputDate) && inputDate.matches(Constants.REGEX_PATTERN_yyyyMMdd));
		}
		return false;
	}

	/**
	 * This method creates and returns the calendar date object with the given
	 * input date in text format.
	 * 
	 * @param dateText
	 *            (Given input date in text format {@link String})
	 * @return (Return calendar object {@link Calendar}})
	 */
	public static Calendar getCalendar(String dateText) {
		// Declare a calendar object.
		Calendar calendar = null;

		// Set default time zone to UTC time zone.
		TimeZone.setDefault(TimeZone.getTimeZone(Constants.UTC_TIMEZONE));

		// Get the current instance of calendar.
		calendar = Calendar.getInstance();
		calendar.clear();

		String date[] = dateText.split(Constants.HYPHEN);
		// Getting year, month and day from the string date.
		int year = Integer.valueOf(date[0]);
		int month = Integer.valueOf(date[1]) + 1;
		System.out.println("Inside getCalendar method value of month :"+month);
		int day = Integer.valueOf(date[2]);
		// Creating calendar with the given input.
		calendar.set(year, month, day);
		return calendar;
	}

	/**
	 * This method creates and returns the calendar date object with the given
	 * input date in text format.
	 * 
	 * @param dateText
	 *            (Given input date in text format {@link String})
	 * @return (Return calendar object {@link Calendar}})
	 */
	public static Calendar getCalendarYYYYMMDD(String dateText) {
		// Declare a calendar object.
		Calendar calendar = null;

		// Set default time zone to UTC time zone.
		TimeZone.setDefault(TimeZone.getTimeZone(Constants.UTC_TIMEZONE));

		// Get the current instance of calendar.
		calendar = Calendar.getInstance();
		calendar.clear();

		String date[] = dateText.split(Constants.HYPHEN);
		// Getting year, month and day from the string date.
		int year = Integer.valueOf(date[0]);
		int day = Integer.valueOf(date[1]);
		int month = Integer.valueOf(date[2]) - 1;
		System.out.println("Inside getCalendar method value of month :"+month);
		// Creating calendar with the given input.
		calendar.set(year, month, day);
		Date dateUtils = new Date(calendar.getTimeInMillis());
		System.out.println(dateText+" : Get date in utils "+dateUtils);
		return calendar;
	}
	public static String getDateInMilliSec(Date timeStamp){
		SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT_MMMDDYYYYHHMMSS);
		String date = formatter.format(timeStamp);
		try{
		
			return Long.toString(formatter.parse(date).getTime());
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
				
	}
}