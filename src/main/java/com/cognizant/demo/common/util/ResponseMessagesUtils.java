package com.cognizant.demo.common.util;

/**
 * This is a utility class containing the messages required to send in response.
 * 
 * @author TarunKumar.Singh
 *
 */
public interface ResponseMessagesUtils {

	public static final String SUCCESS = "Success";
	public static final String ERROR = "Error";
	
}
